﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModifyString
{
    class Program
    {
        public static void Main(string[] args)
        {
            Student s1 = new Student();
            s1.Name = "Alexandr";
            Student s2 = new Student();
            s2.Name = s1.Name;
            s2.Name = s1.Name.ToUpper();
            Console.WriteLine("s1 - " + s1.Name + ", s2 - " + s2.Name);
            Console.WriteLine("Нажмите Enter для завершения программы");
            Console.Read();
        }
    }
  class Student
    {
        public String Name;
    }
}
